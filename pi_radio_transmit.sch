EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HCT00 U?
U 1 1 5F79F506
P 6500 3000
AR Path="/5F9B9AC0/5F79F506" Ref="U?"  Part="1" 
AR Path="/5F747C1C/5F79F506" Ref="U701"  Part="1" 
F 0 "U701" H 6500 3325 50  0000 C CNN
F 1 "74HCT00" H 6500 3234 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6500 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hct00" H 6500 3000 50  0001 C CNN
	1    6500 3000
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74HCT00 U?
U 2 1 5F79F50C
P 5800 3000
AR Path="/5F9B9AC0/5F79F50C" Ref="U?"  Part="2" 
AR Path="/5F747C1C/5F79F50C" Ref="U701"  Part="2" 
F 0 "U701" H 5800 3325 50  0000 C CNN
F 1 "74HCT00" H 5800 3234 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5800 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hct00" H 5800 3000 50  0001 C CNN
	2    5800 3000
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74HCT00 U?
U 3 1 5F79F512
P 7400 3450
AR Path="/5F9B9AC0/5F79F512" Ref="U?"  Part="3" 
AR Path="/5F747C1C/5F79F512" Ref="U701"  Part="3" 
F 0 "U701" H 7400 3133 50  0000 C CNN
F 1 "74HCT00" H 7400 3224 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 7400 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hct00" H 7400 3450 50  0001 C CNN
	3    7400 3450
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74HCT00 U?
U 4 1 5F79F518
P 6500 2400
AR Path="/5F9B9AC0/5F79F518" Ref="U?"  Part="4" 
AR Path="/5F747C1C/5F79F518" Ref="U701"  Part="4" 
F 0 "U701" H 6500 2725 50  0000 C CNN
F 1 "74HCT00" H 6500 2634 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6500 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hct00" H 6500 2400 50  0001 C CNN
	4    6500 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	6150 3100 6100 3100
Wire Wire Line
	6150 2900 6100 2900
$Comp
L Device:C C?
U 1 1 5F79F520
P 5300 3000
AR Path="/5F9B9AC0/5F79F520" Ref="C?"  Part="1" 
AR Path="/5F747C1C/5F79F520" Ref="C702"  Part="1" 
F 0 "C702" V 5048 3000 50  0000 C CNN
F 1 "10n" V 5139 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5338 2850 50  0001 C CNN
F 3 "~" H 5300 3000 50  0001 C CNN
	1    5300 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 3000 5500 3000
Wire Wire Line
	6150 3100 6150 3000
Wire Wire Line
	6200 3000 6150 3000
Connection ~ 6150 3000
Wire Wire Line
	6150 3000 6150 2900
Wire Wire Line
	7700 3350 7800 3350
Wire Wire Line
	7800 3350 7800 3550
Wire Wire Line
	7800 3550 7700 3550
Text GLabel 7850 3350 2    50   Input ~ 0
RX_EN
Wire Wire Line
	7800 3350 7850 3350
Connection ~ 7800 3350
Wire Wire Line
	7000 2900 6800 2900
Wire Wire Line
	6800 3100 6800 3450
$Comp
L power:+5V #PWR?
U 1 1 5F79F533
P 6850 2250
AR Path="/5F9B9AC0/5F79F533" Ref="#PWR?"  Part="1" 
AR Path="/5F747C1C/5F79F533" Ref="#PWR0703"  Part="1" 
F 0 "#PWR0703" H 6850 2100 50  0001 C CNN
F 1 "+5V" H 6865 2423 50  0000 C CNN
F 2 "" H 6850 2250 50  0001 C CNN
F 3 "" H 6850 2250 50  0001 C CNN
	1    6850 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2300 6850 2300
Wire Wire Line
	6850 2300 6850 2250
Wire Wire Line
	6850 2300 6850 2500
Wire Wire Line
	6850 2500 6800 2500
Connection ~ 6850 2300
NoConn ~ 6200 2400
$Comp
L dk_Transistors-FETs-MOSFETs-Single:BS170 Q?
U 1 1 5F79F548
P 4750 3200
AR Path="/5F9B9AC0/5F79F548" Ref="Q?"  Part="1" 
AR Path="/5F747C1C/5F79F548" Ref="Q704"  Part="1" 
F 0 "Q704" H 4858 3253 60  0000 L CNN
F 1 "BS170" H 4858 3147 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3" H 4950 3400 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4950 3500 60  0001 L CNN
F 4 "BS170-ND" H 4950 3600 60  0001 L CNN "Digi-Key_PN"
F 5 "BS170" H 4950 3700 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 4950 3800 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 4950 3900 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4950 4000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BS170/BS170-ND/244280" H 4950 4100 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 60V 500MA TO-92" H 4950 4200 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 4950 4300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4950 4400 60  0001 L CNN "Status"
	1    4750 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3000 5150 3000
$Comp
L dk_Transistors-FETs-MOSFETs-Single:BS170 Q?
U 1 1 5F79F558
P 4450 5400
AR Path="/5F9B9AC0/5F79F558" Ref="Q?"  Part="1" 
AR Path="/5F747C1C/5F79F558" Ref="Q703"  Part="1" 
F 0 "Q703" H 4557 5453 60  0000 L CNN
F 1 "BS170" H 4557 5347 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3" H 4650 5600 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4650 5700 60  0001 L CNN
F 4 "BS170-ND" H 4650 5800 60  0001 L CNN "Digi-Key_PN"
F 5 "BS170" H 4650 5900 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 4650 6000 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 4650 6100 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4650 6200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BS170/BS170-ND/244280" H 4650 6300 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 60V 500MA TO-92" H 4650 6400 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 4650 6500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4650 6600 60  0001 L CNN "Status"
	1    4450 5400
	-1   0    0    -1  
$EndComp
$Comp
L dk_Transistors-FETs-MOSFETs-Single:BS170 Q?
U 1 1 5F79F567
P 4450 4800
AR Path="/5F9B9AC0/5F79F567" Ref="Q?"  Part="1" 
AR Path="/5F747C1C/5F79F567" Ref="Q702"  Part="1" 
F 0 "Q702" H 4557 4853 60  0000 L CNN
F 1 "BS170" H 4557 4747 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3" H 4650 5000 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4650 5100 60  0001 L CNN
F 4 "BS170-ND" H 4650 5200 60  0001 L CNN "Digi-Key_PN"
F 5 "BS170" H 4650 5300 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 4650 5400 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 4650 5500 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4650 5600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BS170/BS170-ND/244280" H 4650 5700 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 60V 500MA TO-92" H 4650 5800 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 4650 5900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4650 6000 60  0001 L CNN "Status"
	1    4450 4800
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F79F56D
P 4250 3300
AR Path="/5F9B9AC0/5F79F56D" Ref="R?"  Part="1" 
AR Path="/5F747C1C/5F79F56D" Ref="R702"  Part="1" 
F 0 "R702" V 4043 3300 50  0000 C CNN
F 1 "10k" V 4134 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4180 3300 50  0001 C CNN
F 3 "~" H 4250 3300 50  0001 C CNN
	1    4250 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F79F573
P 3750 3300
AR Path="/5F9B9AC0/5F79F573" Ref="R?"  Part="1" 
AR Path="/5F747C1C/5F79F573" Ref="R701"  Part="1" 
F 0 "R701" V 3543 3300 50  0000 C CNN
F 1 "10k" V 3634 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 3300 50  0001 C CNN
F 3 "~" H 3750 3300 50  0001 C CNN
	1    3750 3300
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F79F579
P 4000 3500
AR Path="/5F9B9AC0/5F79F579" Ref="C?"  Part="1" 
AR Path="/5F747C1C/5F79F579" Ref="C701"  Part="1" 
F 0 "C701" H 4115 3546 50  0000 L CNN
F 1 "10n" H 4115 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4038 3350 50  0001 C CNN
F 3 "~" H 4000 3500 50  0001 C CNN
	1    4000 3500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5F79F57F
P 4000 3650
AR Path="/5F9B9AC0/5F79F57F" Ref="#PWR?"  Part="1" 
AR Path="/5F747C1C/5F79F57F" Ref="#PWR0702"  Part="1" 
F 0 "#PWR0702" H 4000 3400 50  0001 C CNN
F 1 "Earth" H 4000 3500 50  0001 C CNN
F 2 "" H 4000 3650 50  0001 C CNN
F 3 "~" H 4000 3650 50  0001 C CNN
	1    4000 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3300 4000 3300
Wire Wire Line
	4000 3350 4000 3300
Connection ~ 4000 3300
Wire Wire Line
	4000 3300 4100 3300
Text GLabel 3500 3300 0    50   Input ~ 0
PWM0
Wire Wire Line
	3500 3300 3600 3300
Text GLabel 3500 5200 0    50   Output ~ 0
FILTER_TX
Wire Wire Line
	3700 5200 3500 5200
$Comp
L dk_Transistors-FETs-MOSFETs-Single:BS170 Q?
U 1 1 5F79F596
P 4450 4150
AR Path="/5F9B9AC0/5F79F596" Ref="Q?"  Part="1" 
AR Path="/5F747C1C/5F79F596" Ref="Q701"  Part="1" 
F 0 "Q701" H 4557 4203 60  0000 L CNN
F 1 "BS170" H 4557 4097 60  0000 L CNN
F 2 "digikey-footprints:TO-92-3" H 4650 4350 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4650 4450 60  0001 L CNN
F 4 "BS170-ND" H 4650 4550 60  0001 L CNN "Digi-Key_PN"
F 5 "BS170" H 4650 4650 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 4650 4750 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 4650 4850 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/BS170-D.PDF" H 4650 4950 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/BS170/BS170-ND/244280" H 4650 5050 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 60V 500MA TO-92" H 4650 5150 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 4650 5250 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4650 5350 60  0001 L CNN "Status"
	1    4450 4150
	-1   0    0    -1  
$EndComp
Connection ~ 4750 4250
Wire Wire Line
	4750 3400 4750 4250
Connection ~ 4750 4900
Wire Wire Line
	4750 4900 4750 5500
Wire Wire Line
	4750 4250 4750 4900
$Comp
L power:Earth #PWR?
U 1 1 5F79F5A1
P 3950 5650
AR Path="/5F9B9AC0/5F79F5A1" Ref="#PWR?"  Part="1" 
AR Path="/5F747C1C/5F79F5A1" Ref="#PWR0701"  Part="1" 
F 0 "#PWR0701" H 3950 5400 50  0001 C CNN
F 1 "Earth" H 3950 5500 50  0001 C CNN
F 2 "" H 3950 5650 50  0001 C CNN
F 3 "~" H 3950 5650 50  0001 C CNN
	1    3950 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5650 3950 5600
Wire Wire Line
	3950 4350 4450 4350
Wire Wire Line
	4450 5000 3950 5000
Connection ~ 3950 5000
Wire Wire Line
	3950 5000 3950 4350
Wire Wire Line
	4450 5600 3950 5600
Connection ~ 3950 5600
Wire Wire Line
	3950 5600 3950 5000
Wire Wire Line
	4450 5200 3700 5200
Connection ~ 3700 5200
Wire Wire Line
	4450 4600 3700 4600
Connection ~ 3700 4600
Wire Wire Line
	3700 4600 3700 5200
Wire Wire Line
	4450 3950 3700 3950
Wire Wire Line
	3700 3950 3700 4600
Wire Wire Line
	7100 3450 6800 3450
Wire Notes Line
	6900 2800 6900 3400
Wire Notes Line
	6900 3400 5450 3400
Wire Notes Line
	5450 3400 5450 2800
Wire Notes Line
	5450 2800 6900 2800
Text Notes 5450 3500 0    50   ~ 0
MOSFET Driver
Wire Notes Line
	8150 3650 8150 3050
Wire Notes Line
	8150 3050 7100 3050
Wire Notes Line
	7100 3050 7100 3650
Wire Notes Line
	7100 3650 8150 3650
Text Notes 7100 3750 0    50   ~ 0
Transmit Disable
Wire Notes Line
	3550 5850 4850 5850
Wire Notes Line
	4850 5850 4850 3850
Wire Notes Line
	4850 3850 3550 3850
Wire Notes Line
	3550 3850 3550 5850
Text Notes 3550 5950 0    50   ~ 0
Class C Amplifier
Wire Wire Line
	4400 3300 4450 3300
Text GLabel 7000 2900 2    50   Input ~ 0
Si5351_2
$EndSCHEMATC
