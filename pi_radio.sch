EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 7
Title "uSDX Pi Radio"
Date "2020-09-22"
Rev "0.1"
Comp "Offline Systems LLC"
Comment1 "Andrew Roy Jackman"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Raspberry_Pi_2_3 J103
U 1 1 5F6A1296
P 4450 3200
F 0 "J103" H 4450 4681 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 4450 4590 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 4450 3200 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 4450 3200 50  0001 C CNN
	1    4450 3200
	1    0    0    -1  
$EndComp
Text GLabel 5250 3900 2    50   Output ~ 0
PWM0
$Comp
L power:Earth #PWR0103
U 1 1 5F93E8EE
P 4050 4500
F 0 "#PWR0103" H 4050 4250 50  0001 C CNN
F 1 "Earth" H 4050 4350 50  0001 C CNN
F 2 "" H 4050 4500 50  0001 C CNN
F 3 "~" H 4050 4500 50  0001 C CNN
	1    4050 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0104
U 1 1 5F93F445
P 4150 4500
F 0 "#PWR0104" H 4150 4250 50  0001 C CNN
F 1 "Earth" H 4150 4350 50  0001 C CNN
F 2 "" H 4150 4500 50  0001 C CNN
F 3 "~" H 4150 4500 50  0001 C CNN
	1    4150 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0105
U 1 1 5F93F71B
P 4250 4500
F 0 "#PWR0105" H 4250 4250 50  0001 C CNN
F 1 "Earth" H 4250 4350 50  0001 C CNN
F 2 "" H 4250 4500 50  0001 C CNN
F 3 "~" H 4250 4500 50  0001 C CNN
	1    4250 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0106
U 1 1 5F93FB05
P 4350 4500
F 0 "#PWR0106" H 4350 4250 50  0001 C CNN
F 1 "Earth" H 4350 4350 50  0001 C CNN
F 2 "" H 4350 4500 50  0001 C CNN
F 3 "~" H 4350 4500 50  0001 C CNN
	1    4350 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0107
U 1 1 5F93FE7C
P 4450 4500
F 0 "#PWR0107" H 4450 4250 50  0001 C CNN
F 1 "Earth" H 4450 4350 50  0001 C CNN
F 2 "" H 4450 4500 50  0001 C CNN
F 3 "~" H 4450 4500 50  0001 C CNN
	1    4450 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0108
U 1 1 5F940294
P 4550 4500
F 0 "#PWR0108" H 4550 4250 50  0001 C CNN
F 1 "Earth" H 4550 4350 50  0001 C CNN
F 2 "" H 4550 4500 50  0001 C CNN
F 3 "~" H 4550 4500 50  0001 C CNN
	1    4550 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0109
U 1 1 5F940667
P 4650 4500
F 0 "#PWR0109" H 4650 4250 50  0001 C CNN
F 1 "Earth" H 4650 4350 50  0001 C CNN
F 2 "" H 4650 4500 50  0001 C CNN
F 3 "~" H 4650 4500 50  0001 C CNN
	1    4650 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0110
U 1 1 5F940A51
P 4750 4500
F 0 "#PWR0110" H 4750 4250 50  0001 C CNN
F 1 "Earth" H 4750 4350 50  0001 C CNN
F 2 "" H 4750 4500 50  0001 C CNN
F 3 "~" H 4750 4500 50  0001 C CNN
	1    4750 4500
	1    0    0    -1  
$EndComp
$Sheet
S 600  7000 1250 850 
U 5F99CF6C
F0 "Power" 50
F1 "pi_radio_power.sch" 50
$EndSheet
Text GLabel 5250 2600 2    50   BiDi ~ 0
I2C_SDA
Text GLabel 5250 2700 2    50   Output ~ 0
I2C_SCL
$Sheet
S 5650 2550 1050 200 
U 5F9B9AC0
F0 "Mixer" 50
F1 "pi_radio_mixer.sch" 50
$EndSheet
Text GLabel 5250 3600 2    50   Input ~ 0
SPI_MOSI
Text GLabel 5250 3500 2    50   Output ~ 0
SPI_MISO
Text GLabel 5250 3700 2    50   Output ~ 0
SPI_SCLK
Text GLabel 5250 3400 2    50   Output ~ 0
SPI_CE0
$Sheet
S 5650 3350 1050 350 
U 5F6E40C0
F0 "ADC" 50
F1 "pi_radio_adc.sch" 50
$EndSheet
Text GLabel 3650 2600 0    50   Output ~ 0
RX_EN
NoConn ~ 5250 3300
NoConn ~ 5250 3100
NoConn ~ 5250 3000
NoConn ~ 5250 2900
NoConn ~ 5250 2400
NoConn ~ 3650 2300
NoConn ~ 3650 2400
NoConn ~ 5250 2300
$Sheet
S 6750 2550 1050 200 
U 5F725BA6
F0 "Filter" 50
F1 "pi_radio_filter.sch" 50
$EndSheet
NoConn ~ 3650 3200
Text GLabel 3650 3600 0    50   Input ~ 0
ROT0_A
Text GLabel 3650 3700 0    50   Input ~ 0
ROT0_B
$Comp
L power:Earth #PWR0111
U 1 1 5F762561
P 4650 6100
F 0 "#PWR0111" H 4650 5850 50  0001 C CNN
F 1 "Earth" H 4650 5950 50  0001 C CNN
F 2 "" H 4650 6100 50  0001 C CNN
F 3 "~" H 4650 6100 50  0001 C CNN
	1    4650 6100
	1    0    0    -1  
$EndComp
Text GLabel 5050 5850 0    50   Output ~ 0
ROT0_A
Wire Wire Line
	5050 5950 4650 5950
Wire Wire Line
	4650 5950 4650 6100
Text GLabel 3650 3800 0    50   Input ~ 0
ROT1_A
Text GLabel 3650 3900 0    50   Input ~ 0
ROT1_B
Text GLabel 3650 3400 0    50   Input ~ 0
SW0
$Comp
L power:+3V3 #PWR0112
U 1 1 5F766773
P 5750 5550
F 0 "#PWR0112" H 5750 5400 50  0001 C CNN
F 1 "+3V3" H 5765 5723 50  0000 C CNN
F 2 "" H 5750 5550 50  0001 C CNN
F 3 "" H 5750 5550 50  0001 C CNN
	1    5750 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5550 5750 5750
$Comp
L power:Earth #PWR0113
U 1 1 5F788703
P 5900 6100
F 0 "#PWR0113" H 5900 5850 50  0001 C CNN
F 1 "Earth" H 5900 5950 50  0001 C CNN
F 2 "" H 5900 6100 50  0001 C CNN
F 3 "~" H 5900 6100 50  0001 C CNN
	1    5900 6100
	1    0    0    -1  
$EndComp
Text GLabel 6300 5850 0    50   Output ~ 0
ROT1_A
Text GLabel 6300 6050 0    50   Output ~ 0
ROT1_B
Wire Wire Line
	6300 5950 5900 5950
Wire Wire Line
	5900 5950 5900 6100
Text GLabel 3650 3500 0    50   Input ~ 0
SW1
Wire Wire Line
	5550 5750 5750 5750
$Comp
L power:+3V3 #PWR0114
U 1 1 5F79ADC3
P 7000 5550
F 0 "#PWR0114" H 7000 5400 50  0001 C CNN
F 1 "+3V3" H 7015 5723 50  0000 C CNN
F 2 "" H 7000 5550 50  0001 C CNN
F 3 "" H 7000 5550 50  0001 C CNN
	1    7000 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 5550 7000 5750
Text GLabel 6300 5750 0    50   Output ~ 0
SW1
Wire Wire Line
	6800 5750 7000 5750
$Sheet
S 1900 7000 550  150 
U 5F747973
F0 "Reception" 50
F1 "pi_radio_receive.sch" 50
$EndSheet
$Sheet
S 2450 5500 1750 800 
U 5F747C1C
F0 "Transmission" 50
F1 "pi_radio_transmit.sch" 50
$EndSheet
$Comp
L power:Earth #PWR0102
U 1 1 5F77DA1B
P 2050 3050
AR Path="/5F77DA1B" Ref="#PWR0102"  Part="1" 
AR Path="/5F9B9AC0/5F77DA1B" Ref="#PWR?"  Part="1" 
F 0 "#PWR0102" H 2050 2800 50  0001 C CNN
F 1 "Earth" H 2050 2900 50  0001 C CNN
F 2 "" H 2050 3050 50  0001 C CNN
F 3 "~" H 2050 3050 50  0001 C CNN
	1    2050 3050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0101
U 1 1 5F77DA21
P 1800 2550
AR Path="/5F77DA21" Ref="#PWR0101"  Part="1" 
AR Path="/5F9B9AC0/5F77DA21" Ref="#PWR?"  Part="1" 
F 0 "#PWR0101" H 1800 2400 50  0001 C CNN
F 1 "+3V3" H 1815 2723 50  0000 C CNN
F 2 "" H 1800 2550 50  0001 C CNN
F 3 "" H 1800 2550 50  0001 C CNN
	1    1800 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 3050 1800 3050
Wire Wire Line
	1800 3050 1800 2550
Text GLabel 2150 3050 3    50   BiDi ~ 0
I2C_SDA
Text GLabel 2250 3050 3    50   Input ~ 0
I2C_SCL
Text GLabel 2550 3050 3    50   Output ~ 0
Si5351_0
Text GLabel 2450 3050 3    50   Output ~ 0
Si5351_1
$Comp
L Connector:Conn_01x07_Female J102
U 1 1 5F77E11C
P 2250 2850
AR Path="/5F77E11C" Ref="J102"  Part="1" 
AR Path="/5F9B9AC0/5F77E11C" Ref="J?"  Part="1" 
F 0 "J102" V 2415 2830 50  0000 C CNN
F 1 "Si5351 Header" V 2324 2830 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 2250 2850 50  0001 C CNN
F 3 "~" H 2250 2850 50  0001 C CNN
	1    2250 2850
	0    -1   -1   0   
$EndComp
Text GLabel 2350 3050 3    50   Output ~ 0
Si5351_2
$Comp
L power:+3V3 #PWR0115
U 1 1 5F74CF74
P 4650 1050
F 0 "#PWR0115" H 4650 900 50  0001 C CNN
F 1 "+3V3" H 4665 1223 50  0000 C CNN
F 2 "" H 4650 1050 50  0001 C CNN
F 3 "" H 4650 1050 50  0001 C CNN
	1    4650 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1900 4650 1900
$Comp
L dk_Encoders:PEC11R-4215F-S0024 S101
U 1 1 5F6F3C89
P 5350 5850
F 0 "S101" H 5300 6222 60  0000 C CNN
F 1 "PEC11R-4215F-S0024" H 5300 6116 60  0000 C CNN
F 2 "digikey-footprints:Rotary_Encoder_Switched_PEC11R" H 5550 6050 60  0001 L CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/PEC11R.pdf" H 5550 6150 60  0001 L CNN
F 4 "PEC11R-4215F-S0024-ND" H 5550 6250 60  0001 L CNN "Digi-Key_PN"
F 5 "PEC11R-4215F-S0024" H 5550 6350 60  0001 L CNN "MPN"
F 6 "Sensors, Transducers" H 5550 6450 60  0001 L CNN "Category"
F 7 "Encoders" H 5550 6550 60  0001 L CNN "Family"
F 8 "https://www.bourns.com/docs/Product-Datasheets/PEC11R.pdf" H 5550 6650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/bourns-inc/PEC11R-4215F-S0024/PEC11R-4215F-S0024-ND/4499665" H 5550 6750 60  0001 L CNN "DK_Detail_Page"
F 10 "ROTARY ENCODER MECHANICAL 24PPR" H 5550 6850 60  0001 L CNN "Description"
F 11 "Bourns Inc." H 5550 6950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5550 7050 60  0001 L CNN "Status"
	1    5350 5850
	1    0    0    -1  
$EndComp
Text GLabel 5050 5750 0    50   Output ~ 0
SW0
Text GLabel 5050 6050 0    50   Output ~ 0
ROT0_B
$Comp
L dk_Encoders:PEC11R-4215F-S0024 S102
U 1 1 5F6FA71D
P 6600 5850
F 0 "S102" H 6550 6222 60  0000 C CNN
F 1 "PEC11R-4215F-S0024" H 6550 6116 60  0000 C CNN
F 2 "digikey-footprints:Rotary_Encoder_Switched_PEC11R" H 6800 6050 60  0001 L CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/PEC11R.pdf" H 6800 6150 60  0001 L CNN
F 4 "PEC11R-4215F-S0024-ND" H 6800 6250 60  0001 L CNN "Digi-Key_PN"
F 5 "PEC11R-4215F-S0024" H 6800 6350 60  0001 L CNN "MPN"
F 6 "Sensors, Transducers" H 6800 6450 60  0001 L CNN "Category"
F 7 "Encoders" H 6800 6550 60  0001 L CNN "Family"
F 8 "https://www.bourns.com/docs/Product-Datasheets/PEC11R.pdf" H 6800 6650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/bourns-inc/PEC11R-4215F-S0024/PEC11R-4215F-S0024-ND/4499665" H 6800 6750 60  0001 L CNN "DK_Detail_Page"
F 10 "ROTARY ENCODER MECHANICAL 24PPR" H 6800 6850 60  0001 L CNN "Description"
F 11 "Bourns Inc." H 6800 6950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6800 7050 60  0001 L CNN "Status"
	1    6600 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:L L101
U 1 1 5F6F6449
P 4650 1400
F 0 "L101" H 4702 1446 50  0000 L CNN
F 1 "10u" H 4702 1355 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 4650 1400 50  0001 C CNN
F 3 "~" H 4650 1400 50  0001 C CNN
	1    4650 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C101
U 1 1 5F6F8406
P 4950 1200
F 0 "C101" V 4698 1200 50  0000 C CNN
F 1 "10u" V 4789 1200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4988 1050 50  0001 C CNN
F 3 "~" H 4950 1200 50  0001 C CNN
	1    4950 1200
	0    1    1    0   
$EndComp
$Comp
L Device:C C102
U 1 1 5F6F87EA
P 4950 1600
F 0 "C102" V 4698 1600 50  0000 C CNN
F 1 "10u" V 4789 1600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4988 1450 50  0001 C CNN
F 3 "~" H 4950 1600 50  0001 C CNN
	1    4950 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 1200 4650 1200
Wire Wire Line
	4650 1200 4650 1250
Wire Wire Line
	4650 1050 4650 1200
Connection ~ 4650 1200
Wire Wire Line
	5100 1200 5100 1600
Wire Wire Line
	4650 1550 4650 1600
Wire Wire Line
	4650 1600 4800 1600
Wire Wire Line
	4650 1600 4650 1900
Connection ~ 4650 1600
Connection ~ 4650 1900
$Comp
L power:Earth #PWR0117
U 1 1 5F6FDAB4
P 5100 1600
AR Path="/5F6FDAB4" Ref="#PWR0117"  Part="1" 
AR Path="/5F9B9AC0/5F6FDAB4" Ref="#PWR?"  Part="1" 
F 0 "#PWR0117" H 5100 1350 50  0001 C CNN
F 1 "Earth" H 5100 1450 50  0001 C CNN
F 2 "" H 5100 1600 50  0001 C CNN
F 3 "~" H 5100 1600 50  0001 C CNN
	1    5100 1600
	1    0    0    -1  
$EndComp
Connection ~ 5100 1600
$Comp
L power:+5V #PWR0116
U 1 1 5F6FED57
P 4250 1450
F 0 "#PWR0116" H 4250 1300 50  0001 C CNN
F 1 "+5V" H 4265 1623 50  0000 C CNN
F 2 "" H 4250 1450 50  0001 C CNN
F 3 "" H 4250 1450 50  0001 C CNN
	1    4250 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1450 4250 1900
Wire Wire Line
	4250 1900 4350 1900
Connection ~ 4250 1900
$EndSCHEMATC
