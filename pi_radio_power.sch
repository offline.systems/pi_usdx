EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Barrel-Power-Connectors:PJ-102A J201
U 1 1 5F99DAF5
P 2100 2000
F 0 "J201" H 2033 2225 50  0000 C CNN
F 1 "PJ-102A" H 2033 2134 50  0000 C CNN
F 2 "digikey-footprints:Barrel_Jack_5.5mmODx2.1mmID_PJ-102A" H 2300 2200 60  0001 L CNN
F 3 "https://www.cui.com/product/resource/digikeypdf/pj-102a.pdf" H 2300 2300 60  0001 L CNN
F 4 "CP-102A-ND" H 2300 2400 60  0001 L CNN "Digi-Key_PN"
F 5 "PJ-102A" H 2300 2500 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 2300 2600 60  0001 L CNN "Category"
F 7 "Barrel - Power Connectors" H 2300 2700 60  0001 L CNN "Family"
F 8 "https://www.cui.com/product/resource/digikeypdf/pj-102a.pdf" H 2300 2800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cui-inc/PJ-102A/CP-102A-ND/275425" H 2300 2900 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN PWR JACK 2X5.5MM SOLDER" H 2300 3000 60  0001 L CNN "Description"
F 11 "CUI Inc." H 2300 3100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2300 3200 60  0001 L CNN "Status"
	1    2100 2000
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0201
U 1 1 5F9A323B
P 2200 2200
F 0 "#PWR0201" H 2200 1950 50  0001 C CNN
F 1 "Earth" H 2200 2050 50  0001 C CNN
F 2 "" H 2200 2200 50  0001 C CNN
F 3 "~" H 2200 2200 50  0001 C CNN
	1    2200 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C202
U 1 1 5F9A7627
P 4200 2150
F 0 "C202" H 4315 2196 50  0000 L CNN
F 1 "10u" H 4315 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4238 2000 50  0001 C CNN
F 3 "~" H 4200 2150 50  0001 C CNN
	1    4200 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 2300 4200 2300
Wire Wire Line
	4000 2000 4200 2000
$Comp
L Device:C C201
U 1 1 5F9A7CB1
P 3150 2150
F 0 "C201" H 3265 2196 50  0000 L CNN
F 1 "10u" H 3265 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3188 2000 50  0001 C CNN
F 3 "~" H 3150 2150 50  0001 C CNN
	1    3150 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2300 3700 2300
Wire Wire Line
	3400 2000 3150 2000
$Comp
L power:Earth #PWR0203
U 1 1 5F9A7DAC
P 3700 2300
F 0 "#PWR0203" H 3700 2050 50  0001 C CNN
F 1 "Earth" H 3700 2150 50  0001 C CNN
F 2 "" H 3700 2300 50  0001 C CNN
F 3 "~" H 3700 2300 50  0001 C CNN
	1    3700 2300
	1    0    0    -1  
$EndComp
Connection ~ 3700 2300
Wire Wire Line
	3150 2000 2950 2000
Connection ~ 3150 2000
$Comp
L dk_Diodes-Rectifiers-Single:SS14 D201
U 1 1 5F9A9621
P 2850 2000
F 0 "D201" H 2900 1785 50  0000 C CNN
F 1 "SS14" H 2900 1876 50  0000 C CNN
F 2 "digikey-footprints:DO-214AC" H 3050 2200 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/SS19-D.PDF" H 3050 2300 60  0001 L CNN
F 4 "SS14CT-ND" H 3050 2400 60  0001 L CNN "Digi-Key_PN"
F 5 "SS14" H 3050 2500 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 3050 2600 60  0001 L CNN "Category"
F 7 "Diodes - Rectifiers - Single" H 3050 2700 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/SS19-D.PDF" H 3050 2800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/SS14/SS14CT-ND/965729" H 3050 2900 60  0001 L CNN "DK_Detail_Page"
F 10 "DIODE SCHOTTKY 40V 1A SMA" H 3050 3000 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 3050 3100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3050 3200 60  0001 L CNN "Status"
	1    2850 2000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2200 2000 2500 2000
$Comp
L power:+12V #PWR0202
U 1 1 5F9ABBA8
P 2500 2000
F 0 "#PWR0202" H 2500 1850 50  0001 C CNN
F 1 "+12V" H 2515 2173 50  0000 C CNN
F 2 "" H 2500 2000 50  0001 C CNN
F 3 "" H 2500 2000 50  0001 C CNN
	1    2500 2000
	1    0    0    -1  
$EndComp
Connection ~ 2500 2000
Wire Wire Line
	2500 2000 2650 2000
Wire Notes Line
	3000 1650 3000 2450
Wire Notes Line
	3000 2450 4550 2450
Wire Notes Line
	4550 2450 4550 1650
Wire Notes Line
	4550 1650 3000 1650
Text Notes 3600 2550 2    50   ~ 0
5V Regulation
NoConn ~ 2200 2100
$Comp
L power:+5V #PWR0204
U 1 1 5F8401AC
P 4200 2000
F 0 "#PWR0204" H 4200 1850 50  0001 C CNN
F 1 "+5V" H 4215 2173 50  0000 C CNN
F 2 "" H 4200 2000 50  0001 C CNN
F 3 "" H 4200 2000 50  0001 C CNN
	1    4200 2000
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM1117-5.0 U201
U 1 1 5F840EB6
P 3700 2000
F 0 "U201" H 3700 2242 50  0000 C CNN
F 1 "LM1117-5.0" H 3700 2151 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 3700 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 3700 2000 50  0001 C CNN
	1    3700 2000
	1    0    0    -1  
$EndComp
Connection ~ 4200 2000
$EndSCHEMATC
